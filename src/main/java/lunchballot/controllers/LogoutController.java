package lunchballot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/*
  For some reason, adding a .and.logout().invalidateSession(true).logoutUrl("/logout") to
  WebSecurityConfig:configure(HttpSecurity) wasn't enough. So ok, let's do it the hard way.
 */
@Controller
public class LogoutController {

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();

        return "redirect:/login";
    }
}
