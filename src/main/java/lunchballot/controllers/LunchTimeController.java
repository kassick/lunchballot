package lunchballot.controllers;

import lunchballot.ballot.BallotManager;
import lunchballot.ballot.model.Restaurant;
import lunchballot.time.LunchTimeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.Optional;

@Controller
public class LunchTimeController {

    //private Logger log;
    //private LunchTimeManager lunchTimeManager;

    @Autowired
    LunchTimeManager lunchTimeManager;

    @Autowired
    BallotManager ballotManager;

    @GetMapping("/")
    public String getLunchTime(Principal principal, Model model) throws Exception {
        model.addAttribute("username", principal.getName());

        lunchTimeManager.executeSerialized(
                (ltm) -> {

                    boolean lunchTime = ltm.isLunchTime();

                    model.addAttribute("lunchTime", lunchTime);

                    if (!lunchTime) {
                        boolean userHasVoted = ballotManager.userHasVotedToday(principal.getName());
                        model.addAttribute("userHasVoted", userHasVoted);

                        if (!userHasVoted) {
                            model.addAttribute("availableRestaurants", ballotManager.getAvailableRestaurants());
                            model.addAttribute("unavailableRestaurants", ballotManager.getUnavailableRestaurants());
                        }
                    } else {
                        // LunchTime!
                        Optional<Restaurant> chosenOne = ballotManager.getBallotResult();

                        if (chosenOne.isPresent())
                            model.addAttribute("restaurant", chosenOne.get().getName());
                        else
                            model.addAttribute("restaurant", null);

                        model.addAttribute("ballotResults", ballotManager.getBallotResults());
                    }

                    return model;
                }
        );

        return "isItLunchTime";
    }

}
