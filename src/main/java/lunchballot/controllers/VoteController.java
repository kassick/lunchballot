package lunchballot.controllers;

import lunchballot.ballot.BallotManager;
import lunchballot.ballot.model.Vote;
import lunchballot.exceptions.BallotClosedException;
import lunchballot.exceptions.RestaurantUnavailableException;
import lunchballot.exceptions.UnknownUserException;
import lunchballot.exceptions.UserAlreadyVotedException;
import lunchballot.time.LunchTimeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
public class VoteController {
    @Autowired
    BallotManager ballotManager;

    @Autowired
    LunchTimeManager lunchTimeManager;

    @GetMapping("/vote")
    public String registerVote(HttpSession session,
                               Principal principal,
                               Model model,
                               @RequestParam("restaurantId") long restaurantId) throws Exception {
        model.addAttribute("error", null);

        try {
            Vote v =
                    ballotManager.castVoteForUser(
                            principal.getName(),
                            restaurantId);

            model.addAttribute("restaurantName", v.getRestaurant().getName());

        } catch (UnknownUserException e) {

            // Panic: Invalidate and redirect to /

            session.invalidate();
            return "redirect:/";

        } catch (BallotClosedException e) {

            model.addAttribute("error", "A votação já encerrou!");

        } catch (RestaurantUnavailableException e) {

            model.addAttribute("error", "Não é possível votar nesse restaurante");

        } catch (UserAlreadyVotedException e) {

            model.addAttribute("error", "Usuário já votou");
        }

        return "votingResult";
    }

}
