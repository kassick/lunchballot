package lunchballot.controllers;

import lunchballot.LunchBallotApplication;
import lunchballot.ballot.BallotManager;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.LunchTimeManagerImpl;
import lunchballot.time.Week;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.time.LocalDateTime;

@Controller
public class AdminController {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    @Autowired
    BallotManager ballotManager;

    @Autowired
    LunchTimeManager lunchTimeManager;


    private Model populateModel(LunchTimeManager tm,
                                Principal principal,
                                Model model) {
        model.addAttribute("now", tm.now());
        model.addAttribute("username", principal.getName());

        Week w = new Week(tm.today());

        model.addAttribute("weekStart", w.getStart());
        model.addAttribute("weekEnd", w.getEnd());

        model.addAttribute("ballotResults", ballotManager.getBallotResults());

        model.addAttribute("availableRestaurants", ballotManager.getAvailableRestaurants());
        model.addAttribute("unavailableRestaurants", ballotManager.getUnavailableRestaurants());

        return model;
    }

    @GetMapping("/admin")
    public String getAdminPage(Principal principal,
                               Model model) throws Exception {

        lunchTimeManager.executeSerialized(
                (tm) -> {
                    populateModel(tm, principal, model);

                    return true;
                }
        );

        return "adminPanel";
    }

    @PostMapping("/admin")
    public String doAdminAction(Principal principal,
                                Model model,
                                @RequestParam(value = "action", required = true) String action,
                                @RequestParam(value = "hours", required = false) Integer hours,
                                @RequestParam(value = "minutes", required = false) Integer minutes) throws Exception {
        if (action.equals("gotoTomorrow")) {
            log.warn("Asked to time travel to tomorrow");
            lunchTimeManager.executeSerialized(
                    (tm) -> {
                        tm.timeTravelToNextDay();
                        //populateModel(tm, principal, model);

                        return true;
                    }
            );
        } else if (action.equals("setTime") &&
                (hours >= 0 && hours < 24) &&
                (minutes >= 0 && minutes <= 60)) {
            log.warn("Manually adjusting the time to " + hours + ":" + minutes);
            lunchTimeManager.executeSerialized(
                    (tm) -> {
                        LocalDateTime now =
                                tm.now()
                                        .withHour(hours)
                                        .withMinute(minutes)
                                        .withSecond(0);

                        tm.timeTravelTo(now);

                        try {
                            ballotManager.chooseRestaurant();
                        } catch (Exception e) {
                            // Don't care here
                        }

                        //populateModel(tm, principal, model);

                        return true;
                    }
            );
        } else {
            log.warn("Invalid action for post /admin : " + action);
        }

        return "redirect:/admin";
    }
}
