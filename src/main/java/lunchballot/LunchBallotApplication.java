package lunchballot;

import lunchballot.ballot.BallotManager;
import lunchballot.ballot.model.Restaurant;
import lunchballot.ballot.repositories.RestaurantRepository;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.LunchTimeManagerImpl;
import lunchballot.user.model.HungryUser;
import lunchballot.user.repository.HungryUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
public class LunchBallotApplication {

    /* Change to a XML based bean declaration? */
    @Bean(name = "lunchTimeManager")
    @Scope("singleton")
    public LunchTimeManager lunchTimeManager() {
        return new LunchTimeManagerImpl();
    }

    @Bean
    public BallotManager ballotManager() {
        return new BallotManager();
    }

    @Autowired
    PasswordEncoder passwordEncoder;

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(LunchBallotApplication.class);
    }

    @Bean
    public CommandLineRunner startupPopulateUserRepo(HungryUserRepository hungryUserRepository) {
        return (args) -> {

            log.info("Populating user repo");

            populateUserRepository(hungryUserRepository, passwordEncoder);
        };
    }

    @Bean
    public CommandLineRunner startupPopulateRestaurants(RestaurantRepository restaurantRepository) {
        return (args) -> {

            log.info("Populating restaurant repo");

            populateRestaurants(restaurantRepository);
        };
    }

    static void populateUserRepository(HungryUserRepository hungryUserRepository, PasswordEncoder passwordEncoder) {
        for (int i = 1; i <= 5; i++) {
            hungryUserRepository.save(
                    new HungryUser(
                            String.format("hungryUser%d", i),
                            passwordEncoder.encode("senha123"),
                            HungryUser.ROLE_USER
                    )
            );
        }

        hungryUserRepository.save(
                new HungryUser(
                        "admin",
                        passwordEncoder.encode("admin"),
                        HungryUser.ROLE_ADMIN
                )
        );
    }

    static void populateRestaurants(RestaurantRepository restaurantRepository) {
        restaurantRepository.save(new Restaurant("Vitória"));
        restaurantRepository.save(new Restaurant("Silva"));
        restaurantRepository.save(new Restaurant("40 (PUC)"));
        restaurantRepository.save(new Restaurant("Palatus (PUC)"));
        restaurantRepository.save(new Restaurant("Pé de Manga"));
        restaurantRepository.save(new Restaurant("Bar Maza"));
        restaurantRepository.save(new Restaurant("Novo Sabor"));
        restaurantRepository.save(new Restaurant("Severo Garage (PUC)"));
        restaurantRepository.save(new Restaurant("Sushi"));
    }

}
