package lunchballot.user.repository;

import lunchballot.user.model.HungryUser;
import org.springframework.data.repository.CrudRepository;

public interface HungryUserRepository extends CrudRepository<HungryUser, String> {

}
