package lunchballot.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Users")
public class HungryUser {

    public static final int MAX_USERNAME_LEN = 20;

    public static final String ROLE_USER = "USER";

    public static final String ROLE_ADMIN = "ADMIN";

    @Id
    @Size(max = MAX_USERNAME_LEN)
    @Column(name = "name")
    private String name;

    @Column(name = "pwdHash")
    private String pwdHash;

    @Column(name = "role")
    private String role;

    /* Mandatory empty ctor because of persistance */
    public HungryUser() {
    }

    public HungryUser(String name, String pwdHash, String role) {
        this.name = name;
        this.pwdHash = pwdHash;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return String.format(
                "HungryUser [name=%s, role=%s]",
                this.name, this.role);
    }
}
