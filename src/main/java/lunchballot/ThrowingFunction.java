package lunchballot;

@FunctionalInterface
public interface ThrowingFunction<T, R, E extends Exception> {
    R apply(T arg0) throws E;
}
