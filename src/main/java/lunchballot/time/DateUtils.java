package lunchballot.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {
    public
    static LocalDate getMondayBefore(LocalDate d) {
        // Documentation: monday = 1, sunday = 7
        int dow = d.getDayOfWeek().getValue();

        int diff = dow - DayOfWeek.MONDAY.getValue();

        return d.minusDays(diff);
    }

    public static Date localDateToDate(LocalDate date) {
        return Date.from(
                date.atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
    }
}
