package lunchballot.time;


import lombok.Getter;
import lombok.NonNull;

import java.time.LocalDate;
import java.util.Date;

public class Week {
    @Getter
    private final LocalDate start;

    @Getter
    private final LocalDate end;

    public Week(@NonNull LocalDate day) {
        start = DateUtils.getMondayBefore(day);
        end = start.plusDays(6);
    }

    public Date getStartAsDate() {
        return DateUtils.localDateToDate(start);
    }

    public Date getEndAsDate() {
        return DateUtils.localDateToDate(end);
    }


}
