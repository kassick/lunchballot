package lunchballot.time;

import lunchballot.LunchBallotApplication;
import lunchballot.ThrowingFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

@Component
public class LunchTimeManagerImpl
        implements LunchTimeManager {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    public final static LocalTime LUNCH_TIME = LocalTime.of(11, 50);

    private LocalDateTime now;

    public LunchTimeManagerImpl() {
        log.debug("Created new lunchtimeManager");
        now = LocalDateTime.now();
    }

    public synchronized boolean isLunchTime() {
        if (now.toLocalTime().compareTo(LUNCH_TIME) >= 0) {
            return true;
        }

        return false;
    }

    public synchronized LocalDate today() {
        return now.toLocalDate();
    }

    public Date todayAsDate() {
        return DateUtils.localDateToDate(today());
    }


    public synchronized LocalDateTime now() {
        return now;
    }


    public synchronized void timeTravelTo(LocalDateTime _now) {
        log.warn("Time travelling from " + now.toString() + " to " + _now.toString());
        now = _now;
    }

    public synchronized void timeTravelToNextDay() {
        now = now.plusDays(1).toLocalDate().atStartOfDay();
    }

    @Override
    public synchronized <T> T executeSerialized(ThrowingFunction<LunchTimeManagerImpl, T, Exception> f)
            throws Exception {
        return f.apply(this);
    }

}