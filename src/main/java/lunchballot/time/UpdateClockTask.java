package lunchballot.time;

import lunchballot.LunchBallotApplication;
import lunchballot.ballot.BallotManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;

@Configuration
@EnableScheduling
public class UpdateClockTask {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    @Autowired
    LunchTimeManager lunchTimeManager;

    @Autowired
    BallotManager ballotManager;

    @Bean
    public CommandLineRunner scheduleLunchUpdate(TaskScheduler scheduler) {

        /*

          Runs every day at lunch time (11h50)

          This function updates the time and calls chooseRestaurant

          "Cron" -- All cron formats point to minute precision, but here it has second precision

            * * * * * *
            | | | | | |
            | | | | | +-- Day of the Week   (range: 1-7, 1 standing for Monday)
            | | | | +---- Month of the Year (range: 1-12)
            | | | +------ Day of the Month  (range: 1-31)
            | | +-------- Hour              (range: 0-23)
            | +---------- Minute            (range: 0-59)
            +------------ Seconds !!!!!!

        */

        return (args) -> {
            String cronStr = String.format(
                    "0 %d %d * * *",
                    LunchTimeManagerImpl.LUNCH_TIME.getMinute(),
                    LunchTimeManagerImpl.LUNCH_TIME.getHour());

            CronTrigger trigger = new CronTrigger(cronStr);

            scheduler.schedule(this::updateLunchChoice, trigger);

            log.info("Noon updater schedulled");
        };
    }

    @Bean
    public CommandLineRunner scheduleMidnightUpdate(TaskScheduler scheduler) {
        /*

          Runs every midnight to bump the date

        */

        return (args) -> {
            String cronStr = "0 0 0 * * *";

            CronTrigger trigger = new CronTrigger(cronStr);

            scheduler.schedule(this::updateDateAtMidnight, trigger);

            log.info("Midnight updater scheduled");
        };
    }


    private void updateLunchChoice() {
        // Time is 11h50
        LocalDateTime now = LocalDateTime.now();

        try {
            lunchTimeManager.executeSerialized(
                    (tm) -> {
                        tm.timeTravelTo(now);
                        ballotManager.chooseRestaurant();

                        return true;
                    }
            );
        } catch (Exception e) {
            log.error("Could not adjust time: " + e.toString());
        }
    }


    private void updateDateAtMidnight() {
        // time is midnight!
        LocalDateTime now = LocalDateTime.now();

        try {
            lunchTimeManager.executeSerialized(
                    (tm) -> {
                        tm.timeTravelTo(now);

                        return true;
                    }
            );
        } catch (Exception e) {
            log.error("Could not adjust time: " + e.toString());
        }
    }
}
