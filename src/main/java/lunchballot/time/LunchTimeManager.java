package lunchballot.time;

import lunchballot.ThrowingFunction;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public interface LunchTimeManager {
    public boolean isLunchTime();

    public LocalDate today();

    Date todayAsDate();

    public LocalDateTime now();

    public <T> T executeSerialized(ThrowingFunction<LunchTimeManagerImpl, T, Exception> f)
            throws Exception ;

}
