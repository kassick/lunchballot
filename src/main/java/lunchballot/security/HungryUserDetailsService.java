package lunchballot.security;

import lunchballot.LunchBallotApplication;
import lunchballot.user.repository.HungryUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class HungryUserDetailsService implements UserDetailsService {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    @Autowired
    HungryUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        log.info("Asking for auth of " + username);
        HungryUserPrincipal ret =
                userRepository
                        .findById(username)
                        .map((user) -> new HungryUserPrincipal(user))
                        .orElse(null);

        if (ret == null) {
            log.warn("Unknown user " + username);
            throw new UsernameNotFoundException(username);
        }

        log.debug("Found auth for " + ret.getUsername() + " / " + ret.getPassword());

        return ret;
    }
}
