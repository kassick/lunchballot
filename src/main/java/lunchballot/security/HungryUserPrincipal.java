package lunchballot.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lunchballot.user.model.HungryUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

@RequiredArgsConstructor
public class HungryUserPrincipal implements UserDetails {


    @Getter
    private final HungryUser user;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));

        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPwdHash();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        // @TODO : Include this information in the HungryUser entity

        return true;
    }

    public boolean isAccountNonLocked() {
        // @TODO: Include this information in the HungryUser entity

        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // @TODO: Include this information in the HungryUser entity

        return true;
    }

    @Override
    public boolean isEnabled() {
        // @TODO: Include this information in the HungryUser entity

        return true;
    }
}

