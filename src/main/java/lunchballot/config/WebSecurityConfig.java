package lunchballot.config;

import lunchballot.LunchBallotApplication;
import lunchballot.security.HungryUserDetailsService;
import lunchballot.user.model.HungryUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan("lunchballot.security")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    @Autowired
    HungryUserDetailsService userService;

    /*
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest().fullyAuthenticated()
                .and()
                .formLogin();
    }*/

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/h2-console/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/theme/**").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/").hasAnyRole(HungryUser.ROLE_ADMIN, HungryUser.ROLE_USER)
                .antMatchers("/vote").hasAnyRole(HungryUser.ROLE_ADMIN, HungryUser.ROLE_USER)
                .antMatchers("/admin").hasRole(HungryUser.ROLE_ADMIN)
                .and()
                .formLogin();
//            .and()
//                .logout()
//                    .invalidateHttpSession(true)
//                    .logoutUrl("/logout")
//                    .logoutSuccessUrl("/login");

    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("Setting up database backed authentication");
        auth.authenticationProvider(authenticationProvider());
    }
}
/*
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().fullyAuthenticated()
				.and()
			.formLogin();
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.groupSearchBase("ou=groups")
				.contextSource()
					.url("ldap://localhost:8389/dc=springframework,dc=org")
					.and()
				.passwordCompare()
					.passwordEncoder(new LdapShaPasswordEncoder())
					.passwordAttribute("userPassword");
	}

}
 */