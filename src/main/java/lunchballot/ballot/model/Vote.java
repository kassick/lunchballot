package lunchballot.ballot.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "Votes")
public class Vote {

    @EmbeddedId
    final private VoteId voteId;

    @ManyToOne
    @JoinColumn(name = "restaurantId", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    final private Restaurant restaurant;

    public Vote() {
        voteId = null;
        restaurant = null;
    }
}
