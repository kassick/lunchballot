package lunchballot.ballot.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lunchballot.user.model.HungryUser;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@Embeddable
@RequiredArgsConstructor
public class VoteId implements Serializable {
    @NotNull
    @Size(max = HungryUser.MAX_USERNAME_LEN)
    @ManyToOne
    @JoinColumn(name = "userName", referencedColumnName = "name")
    @OnDelete(action = OnDeleteAction.CASCADE)
    final private HungryUser hungryUser;

    @NotNull
    @Column(name = "date")
    @Temporal(value = TemporalType.DATE)
    final private Date date;

    public VoteId() {
        date = null;
        hungryUser = null;
    }
}
