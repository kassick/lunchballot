package lunchballot.ballot.model;

import javax.annotation.Generated;
import javax.persistence.*;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "Restaurants")
public class Restaurant {

    @Column(name = "id")
    private @GeneratedValue
    @Id
    Long id;

    @Column(name = "name")
    private final String name;

    @Override
    public String toString() {
        return String.format("Restaurant [id=%d, name=%s]",
                id, name);
    }

    protected Restaurant() {
        name = null;
        id = null;
    }
}
