package lunchballot.ballot.model;

/* projection from the Votes table */
public interface BallotResultProjection {

    Long getRestaurantId();

    String getRestaurantName();

    Long getNVotes();

    default String asString() {
        return String.format("BallotResultProjection [id=%d, name=%s, votes=%d]",
                getRestaurantId(),
                getRestaurantName(),
                getNVotes());
    }
}
