package lunchballot.ballot.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;

@Data
@RequiredArgsConstructor
@Entity
@Table(name = "BallotResults")
public class BallotResult {

    @Id
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private final Date date;

    // Restaurant can be null if there was no vote for that day
    @Nullable
    @ManyToOne
    @JoinColumn(name = "restaurantId", referencedColumnName = "id", nullable = true)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private final Restaurant restaurant;

    public BallotResult() {
        date = null;
        restaurant = null;
    }
}
