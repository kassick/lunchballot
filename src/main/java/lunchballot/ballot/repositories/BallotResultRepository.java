package lunchballot.ballot.repositories;

import lunchballot.ballot.model.BallotResult;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.Optional;

public interface BallotResultRepository extends CrudRepository<BallotResult, Date> {
    Optional<BallotResult> findByDate(Date date);
}
