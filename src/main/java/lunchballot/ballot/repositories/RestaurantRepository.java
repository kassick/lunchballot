package lunchballot.ballot.repositories;

import lunchballot.ballot.model.Restaurant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {
    /*
      List of restaurants matching some name
     */
    List<Restaurant> findByNameContainingIgnoreCase(String name);

    /*
      Gets a list of restaurants that can still be chosen
      A restaurant can be chosen IFF it has not been chosen in the date bracket (inclusive)
     */
    @Query(value =
            "SELECT * FROM Restaurants R " +
                    "WHERE NOT EXISTS (SELECT * " +
                    "                  FROM Ballot_Results B " +
                    "                  WHERE B.date BETWEEN ?1 AND ?2 AND " +
                    "                  B.restaurant_Id = R.id " +
                    "                 )",
            nativeQuery = true)
    List<Restaurant> getAvailableRestaurants(Date start, Date end);

    /*
      Gets a list of restaurants that can not be chosen
      A restaurant can be chosen IFF it has not been chosen in the date bracket (inclusive)
     */
    @Query(value =
            "SELECT * FROM Restaurants R " +
                    "WHERE EXISTS (SELECT * " +
                    "              FROM Ballot_Results B " +
                    "              WHERE B.date BETWEEN ?1 AND ?2 AND " +
                    "              B.restaurant_Id = R.id " +
                    "             )",
            nativeQuery = true)
    List<Restaurant> getUnavailableRestaurants(Date start, Date end);

    /*
      Returns the registry for a restaurant only if it's available in that period
     */
    @Query(value =
            "SELECT * FROM Restaurants R " +
                    "WHERE R.id = :restid AND " +
                    "      NOT EXISTS (SELECT * " +
                    "                  FROM Ballot_Results B " +
                    "                  WHERE B.restaurant_Id = R.id AND " +
                    "                        B.date >= :start AND " +
                    "                        B.date <= :end " +
                    "                 )",
            nativeQuery = true)
    Optional<Restaurant> getRestaurantIfAvailable(@Param("restid") Long restaurantId,
                                                  @Param("start") @Temporal(value = TemporalType.DATE) Date start,
                                                  @Param("end") @Temporal(value = TemporalType.DATE) Date end
    );
}
