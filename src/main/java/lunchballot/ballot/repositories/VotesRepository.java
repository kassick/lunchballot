package lunchballot.ballot.repositories;

import lunchballot.ballot.model.BallotResultProjection;
import lunchballot.ballot.model.Vote;
import lunchballot.ballot.model.VoteId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface VotesRepository extends CrudRepository<Vote, VoteId> {

    /*
       Gets the (single) ballot result for the specific date
     */
    @Query(value =
            "SELECT R.id AS restaurantId, " +
                    "       R.name AS restaurantName, " +
                    "       COUNT(R.id) AS nVotes " +
                    "FROM Votes V " +
                    "JOIN Restaurants R on V.restaurant_Id = R.id " +
                    "WHERE V.date = ?1 " +
                    "GROUP BY R.id " +
                    "ORDER BY nVotes DESC",
            nativeQuery = true
    )
    List<BallotResultProjection> getBallotResultsFor(Date date);

    /*

      Returns all the (single) vote cast by user on a given date.
       Since the key is username + date, there can be only one record (or none).

       This should be equivalent to
       HungryUser u = userRepository.findById(userName);
       VoteId vid = new VoteId(u, date);
       List<Vote> vs = findById(vid);
       if (vs.empty) {
          return Optional.empty();

       Vote v = vs.first();
       return Optional.of(v);
     */
    Optional<Vote> findByVoteId_hungryUser_nameAndVoteId_date(String userName, Date date);
}
