package lunchballot.ballot;

import lunchballot.LunchBallotApplication;
import lunchballot.ballot.model.*;
import lunchballot.ballot.repositories.BallotResultRepository;
import lunchballot.ballot.repositories.RestaurantRepository;
import lunchballot.ballot.repositories.VotesRepository;
import lunchballot.exceptions.*;
import lunchballot.time.DateUtils;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.Week;
import lunchballot.user.model.HungryUser;
import lunchballot.user.repository.HungryUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class BallotManager {

    public static final Logger log = LoggerFactory.getLogger(LunchBallotApplication.class);

    @Autowired
    private HungryUserRepository userRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private VotesRepository votesRepository;

    @Autowired
    private LunchTimeManager lunchTimeManager;

    @Autowired
    private BallotResultRepository ballotResultRepository;

    /*
      Returns true if the user has voted today
     */
    public boolean userHasVotedToday(String userName) {
        return votesRepository
                .findByVoteId_hungryUser_nameAndVoteId_date(userName, lunchTimeManager.todayAsDate())
                .isPresent();
    }

    public boolean userHasVotedToday(HungryUser user) {
        return userHasVotedToday(user.getName());
    }

    /*
      Casts a vote for a given user. User can vote IFF:
      - It's not lunch time yet
      - It has chosen a restaurant that is available
      - It has not yet voted today

      Returns true if the vote was saved, otherwise it returns false
     */
    @Transactional
    public Vote castVoteForUser(HungryUser user, Restaurant r) throws Exception {
        // Serialize the execution.
        // When the schedulled 11:50 job set's lunchtime to the right time, it can (will?)
        // be executed in a different thread. As a result, userHasVotedToday may use an
        // old Calendar inside the object, and the vote will be cast even if it's (officially)
        // lunch time
        return lunchTimeManager.executeSerialized(
                (tm) -> {
                    if (tm.isLunchTime())
                        throw new BallotClosedException(tm.now());

                    if (!isRestaurantAvailableThisWeek(r))
                        throw new RestaurantUnavailableException(r.getName());

                    if (userHasVotedToday(user.getName()))
                        throw new UserAlreadyVotedException(user.getName(), lunchTimeManager.today());


                    Vote v = new Vote(
                            new VoteId(user, lunchTimeManager.todayAsDate()),
                            r);

                    return votesRepository.save(v);
                }
        );
    }


    @Transactional
    public Vote castVoteForUser(String userName, Restaurant r) throws Exception {
        Optional<HungryUser> ou = userRepository.findById(userName);

        if (ou.isPresent())
            return castVoteForUser(ou.get(), r);
        else
            throw new UnknownUserException(userName);
    }

    @Transactional
    public Vote castVoteForUser(String userName, long restaurantId) throws Exception {
        Optional<HungryUser> ou = userRepository.findById(userName);

        if (!ou.isPresent())
            throw new UnknownUserException(userName);

        Optional<Restaurant> or = restaurantRepository.findById(restaurantId);
        if (!or.isPresent())
            throw new RestaurantUnavailableException(String.format("Unknown restaurant %d", restaurantId));


        return castVoteForUser(ou.get(), or.get());
    }

    @Transactional
    public void chooseRestaurant() throws Exception {
        boolean ok = lunchTimeManager.executeSerialized((tm) -> {
            if (!tm.isLunchTime()) {
                log.warn("choseRestaurant called before lunchtime");
                throw new NotLunchTimeYetException();
            }

            if (ballotResultRepository.findByDate(tm.todayAsDate()).isPresent()) {
                log.warn("chooseRestaurant called when there has already been a choice");
                throw new BallotClosedException(lunchTimeManager.now());
            }

            List<BallotResultProjection> ballotResults =
                    votesRepository.getBallotResultsFor(tm.todayAsDate());

            Restaurant chosenOne = null;
            if (ballotResults.size() == 0) {
                // Ooops, NO ONE HAS CAST A SINGLE FRACKING VOTE!?
                // C'mon!!!

                log.warn("No votes for date " + tm.today().toString());
            } else {
                BallotResultProjection b0 = ballotResults.get(0);
                Restaurant r0 = restaurantRepository.findById(b0.getRestaurantId()).orElse(null);

                if (r0 == null) {
                    // Could only happen once restaurant insertion and deletion is done
                    // How to handle this in an transaction?
                    return false;
                }

                chosenOne = r0;
            }

            ballotResultRepository.save(new BallotResult(tm.todayAsDate(), chosenOne));

            return true;
        });
    }

    /*
      Returns the restaurant chosen for that date

      May return none if:
      - no one voted for that date
      - not lunch time yet
    */

    @Transactional(readOnly = true)
    public Optional<Restaurant> getBallotResultForDate(LocalDate date) {
        Date d = DateUtils.localDateToDate(date);
        Optional<BallotResult> obr = ballotResultRepository.findByDate(d);
        if (!obr.isPresent()) {
            return Optional.empty();
        }

        if (obr.get().getRestaurant() == null)
            return Optional.empty();
        else
            return Optional.of(Objects.requireNonNull(obr.get().getRestaurant()));
    }

    @Transactional(readOnly = true)
    public List<BallotResultProjection> getBallotResultsForDate(LocalDate date) {
        return votesRepository.getBallotResultsFor(DateUtils.localDateToDate(date));
    }

    public List<BallotResultProjection> getBallotResults() {
        return getBallotResultsForDate(lunchTimeManager.today());
    }

    /*
      Returns the ballot result for today
     */
    public Optional<Restaurant> getBallotResult() {
        return getBallotResultForDate(lunchTimeManager.today());
    }

    /*
       Returns the list of restaurants available for today
     */
    public List<Restaurant> getAvailableRestaurants() {

        Week w = new Week(lunchTimeManager.today());

        return restaurantRepository.getAvailableRestaurants(
                w.getStartAsDate(),
                w.getEndAsDate());
    }

    /*
       Returns the list of restaurants that ARE NOT available for today
       (i.e. have already been chosen)
     */
    public List<Restaurant> getUnavailableRestaurants() {
        Week w = new Week(lunchTimeManager.today());

        return restaurantRepository.getUnavailableRestaurants(
                w.getStartAsDate(),
                w.getEndAsDate());
    }

    // -------------------------------------------------------------
    /*

      Returns true if the restaurant has not been chosen this week

     */
    private boolean isRestaurantAvailableThisWeek(Restaurant r) {
        Week w = new Week(lunchTimeManager.today());

        return restaurantRepository
                .getRestaurantIfAvailable(
                        r.getId(),
                        w.getStartAsDate(),
                        w.getEndAsDate())
                .isPresent();
    }
    /*
      Returns a date that's a monday for the given date's week
      getMondayBefore(18-09-2018 (tue)) -> 17-09-2018
     */


}
