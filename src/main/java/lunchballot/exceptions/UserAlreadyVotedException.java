package lunchballot.exceptions;


import java.time.LocalDate;

/*
  Class used to indicate that a vote can't be registered because the user already voted for that date
 */
public class UserAlreadyVotedException extends BaseVotingException {
    public UserAlreadyVotedException(String userName, LocalDate date) {
        super("User " + userName + " already voted on " + date.toString());
    }
}
