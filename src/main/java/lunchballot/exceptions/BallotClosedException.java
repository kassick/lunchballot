package lunchballot.exceptions;

import java.time.LocalDateTime;

/*
  Class used to indicate that a vote can't be registered because the ballot already closed (i.e. it's lunch time)
 */
public class BallotClosedException extends BaseVotingException {
    public BallotClosedException(LocalDateTime now) {
        super("Ballot is closed, vote cast at " + now.toString());
    }
}
