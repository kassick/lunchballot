package lunchballot.exceptions;

/*
  Class used to indicate that a vote can't be cast because the user doesn't exist
 */
public class UnknownUserException extends BaseVotingException {
    public UnknownUserException(String userName) {
        super("The user `" + userName + "' is invalid");
    }
}
