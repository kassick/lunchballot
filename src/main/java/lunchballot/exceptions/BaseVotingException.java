package lunchballot.exceptions;

/*
  Base class for exceptions during voting
 */
public class BaseVotingException extends Exception {
    BaseVotingException(String msg) {
        super(msg);
    }
}
