package lunchballot.exceptions;

public class RestaurantUnavailableException extends BaseVotingException {
    public RestaurantUnavailableException(String name) {
        super("Restaurant `" + name + "' is no longer available this week");
    }
}
