package lunchballot.exceptions;

public class NotLunchTimeYetException extends BaseVotingException
{
    public NotLunchTimeYetException() {
        super("Not lunch time yet");
    }
}
