Feature: Can I vote before it's lunchtime?
  Users should be able to vote before lunch time

  Scenario: hungryUser1 can vote at 11:00
    Given now it's 11:00
    Given the database is freshly populated
    When hungryUser1 attempts to vote 1 time for restaurant Palatus
    Then it has 1 vote in the database
    Then its vote is for restaurant Palatus

  Scenario: hungryUser2 can vote only once before lunch time
    Given now it's 11:15
    Given the database is freshly populated
    When hungryUser2 attempts to vote 1 time for restaurant Palatus
    When hungryUser2 attempts to vote 1 time for restaurant Manga
    Then it has 1 vote in the database
    Then its vote is for restaurant Palatus

  Scenario: hungryUser2 can vote only once before lunch time (2)
    Given now it's 11:15
    Given the database is freshly populated
    When hungryUser2 attempts to vote 1 time for restaurant Manga
    When hungryUser2 attempts to vote 1 time for restaurant Palatus
    Then it has 1 vote in the database
    Then its vote is for restaurant Manga