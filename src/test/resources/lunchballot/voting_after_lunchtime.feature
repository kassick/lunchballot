Feature: After the ballot closes, gotta accept whatever food they serve me
  Users can't vote before lunch time

  Scenario: hungryUser1 can not vote at 12:00
    Given now it's 12:00
    Given the database is freshly populated
    When hungryUser1 attempts to vote 1 time for restaurant Palatus
    Then it has 0 votes in the database
    Then its vote is for restaurant NONE