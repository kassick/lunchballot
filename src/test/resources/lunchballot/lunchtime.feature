Feature: Is it lunch time?
  Everyone wants to know if it's already lunch time

  Scenario: 11:00 is not lunch time
    Given it's 11:00
    When I ask if it's lunch time
    Then I should be told "not yet"

  Scenario: 11:50 is lunch time
    Given it's 11:50
    When I ask if it's lunch time
    Then I should be told "yes"

  Scenario: 13:00 is lunch time
    Given it's 11:50
    When I ask if it's lunch time
    Then I should be told "yes"
    
  Scenario: when the day turns, it's not lunch time
    Given now is midnight
    When I ask if it's lunch time
    Then I should be told "not yet"