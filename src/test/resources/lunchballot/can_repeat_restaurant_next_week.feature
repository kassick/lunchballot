Feature: Fine, we'll eat a prato feito today again
  We can vote on the same restaurant we went last week -- as long as it hasn't been repeated this week

  Scenario: Can go to Silva again next week!!!
    Given the database is freshly populated with 3 users

    # Day 1, Monday
    Given today is 17/09/2018
    Given now it's 11:00

    Then hungryUser1 attempts to vote 1 time for restaurant Silva
    Then hungryUser2 attempts to vote 1 time for restaurant Silva
    Then hungryUser3 attempts to vote 1 time for restaurant Severo

    # Silva wins!
    Given now it's 12:00
    Then the chosen restaurant is Silva

    # Tuesday next week
    Given today is 25/09/2018

    # We can accept a vote for Silva this week
    When hungryUser1 attempts to vote 1 time for restaurant Silva
    Then it has 1 votes in the database

    When hungryUser2 attempts to vote 1 time for restaurant Silva
    Then it has 1 votes in the database

    # Sushi is ok, hasn't been chosen this week!
    When hungryUser3 attempts to vote 1 time for restaurant Sushi
    Then it has 1 vote in the database

    # Silva wins. Again. Yay...
    Given now it's 12:00
    Then the chosen restaurant is Silva

    # Make sure counts are ok
    When the votes for today are calculated
    Then restaurant Silva has 2 votes
    Then restaurant Sushi has 1 votes
