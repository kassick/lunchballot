Feature: The number of votes for each restaurant is correct
  No one likes an election with the wrong results

  Scenario: Palatus 5 vs 40 2 vs Manga 3
    Given the database is freshly populated with 10 users
    Given now it's 10:30
    When hungryUser1 attempts to vote 1 times for restaurant Palatus
    When hungryUser2 attempts to vote 1 times for restaurant Palatus
    When hungryUser3 attempts to vote 1 times for restaurant Palatus
    When hungryUser4 attempts to vote 1 times for restaurant Palatus
    When hungryUser5 attempts to vote 1 times for restaurant Palatus
    When hungryUser6 attempts to vote 1 times for restaurant 40
    When hungryUser7 attempts to vote 1 times for restaurant 40
    When hungryUser8 attempts to vote 1 times for restaurant Manga
    When hungryUser9 attempts to vote 1 times for restaurant Manga
    When hungryUser10 attempts to vote 1 times for restaurant Manga
    When the votes for today are calculated
    Then restaurant Palatus has 5 votes
    Then restaurant 40 has 2 votes
    Then restaurant Manga has 3 votes


  Scenario: Severo 1 vs 40 3 vs Sushi 6 => sushi wins
    Given the database is freshly populated with 10 users
    Given now it's 10:30
    When hungryUser1 attempts to vote 1 times for restaurant Severo
    When hungryUser2 attempts to vote 1 times for restaurant 40
    When hungryUser3 attempts to vote 1 times for restaurant 40
    When hungryUser4 attempts to vote 1 times for restaurant 40
    When hungryUser5 attempts to vote 1 times for restaurant Sushi
    When hungryUser6 attempts to vote 1 times for restaurant Sushi
    When hungryUser7 attempts to vote 1 times for restaurant Sushi
    When hungryUser8 attempts to vote 1 times for restaurant Sushi
    When hungryUser9 attempts to vote 1 times for restaurant Sushi
    When hungryUser10 attempts to vote 1 times for restaurant Sushi
    When the votes for today are calculated
    Then restaurant Severo has 1 votes
    Then restaurant 40 has 3 votes
    Then restaurant Sushi has 6 votes