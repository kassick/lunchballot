Feature: Never Forget Taco Bell
  The choice of a day will be remembered the next one

  Scenario: On 17/09/2018, sushi, on 18/09/2018, silva
    Given today is 17/09/2018
    Given now it's 11:00
    Given the database is freshly populated with 3 users
    When hungryUser1 attempts to vote 1 times for restaurant Sushi
    When hungryUser2 attempts to vote 1 times for restaurant Sushi
    When hungryUser3 attempts to vote 1 times for restaurant Palatus
    Given now it's 12:00
    # Force chosing a restaurant
    Then the chosen restaurant is Sushi
    Given today is 18/09/2018
    Given now it's 11:00
    When hungryUser1 attempts to vote 1 times for restaurant Manga
    When hungryUser2 attempts to vote 1 times for restaurant Silva
    When hungryUser3 attempts to vote 1 times for restaurant Silva
    Given now it's 12:00
    # Force chosing a restaurant
    Then the chosen restaurant is Silva
    # Now test the memory
    Then choice for 17/09/2018 was Sushi
    Then choice for 18/09/2018 was Silva

  Scenario: On 17/09/2018, NONE, on 18/09/2018, silva
    Given today is 17/09/2018
    Given now it's 11:00
    Given the database is freshly populated with 3 users
    Given now it's 12:00
    # Force chosing a restaurant
    Then the chosen restaurant is NONE
    Given today is 18/09/2018
    Given now it's 11:00
    Given the database is freshly populated with 3 users
    When hungryUser1 attempts to vote 1 times for restaurant Manga
    When hungryUser2 attempts to vote 1 times for restaurant Silva
    When hungryUser3 attempts to vote 1 times for restaurant Silva
    Given now it's 12:00
    # Force chosing a restaurant
    Then the chosen restaurant is Silva
    # Now test the memory
    Then choice for 17/09/2018 was NONE
    Then choice for 18/09/2018 was Silva