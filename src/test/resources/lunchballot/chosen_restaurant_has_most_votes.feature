Feature: Democracy works
  The restaurant with the most votes must be the one selected

  Scenario: Palatus 5 vs 40 2 vs Manga 3 => Palatus wins
    Given the database is freshly populated with 10 users
    Given now it's 10:30
    When hungryUser1 attempts to vote 1 times for restaurant Palatus
    When hungryUser2 attempts to vote 1 times for restaurant Palatus
    When hungryUser3 attempts to vote 1 times for restaurant Palatus
    When hungryUser4 attempts to vote 1 times for restaurant Palatus
    When hungryUser5 attempts to vote 1 times for restaurant Palatus
    When hungryUser6 attempts to vote 1 times for restaurant 40
    When hungryUser7 attempts to vote 1 times for restaurant 40
    When hungryUser8 attempts to vote 1 times for restaurant Manga
    When hungryUser9 attempts to vote 1 times for restaurant Manga
    When hungryUser10 attempts to vote 1 times for restaurant Manga
    Given now it's 12:00
    Then the chosen restaurant is Palatus

  Scenario: Severo 1 vs 40 3 vs Sushi 6 => sushi wins
    Given the database is freshly populated with 10 users
    Given now it's 10:30
    When hungryUser1 attempts to vote 1 times for restaurant Severo
    When hungryUser2 attempts to vote 1 times for restaurant 40
    When hungryUser3 attempts to vote 1 times for restaurant 40
    When hungryUser4 attempts to vote 1 times for restaurant 40
    When hungryUser5 attempts to vote 1 times for restaurant Sushi
    When hungryUser6 attempts to vote 1 times for restaurant Sushi
    When hungryUser7 attempts to vote 1 times for restaurant Sushi
    When hungryUser8 attempts to vote 1 times for restaurant Sushi
    When hungryUser9 attempts to vote 1 times for restaurant Sushi
    When hungryUser10 attempts to vote 1 times for restaurant Sushi
    Given now it's 12:00
    Then the chosen restaurant is Sushi

  Scenario: No one cares for lunch :(
    Given the database is freshly populated with 10 users
    Given now it's 10:30
    Given now it's 12:00
    Then the chosen restaurant is NONE