Feature: Lunch time must be a new adventure every day!
  Do not allow voting for restaurants that have already been chosen on the same week

  Scenario: Can't go to Silva again this week!!!
    Given the database is freshly populated with 3 users

    # Day 1
    Given today is 17/09/2018
    Given now it's 11:00

    Then hungryUser1 attempts to vote 1 time for restaurant Silva
    Then hungryUser2 attempts to vote 1 time for restaurant Silva
    Then hungryUser3 attempts to vote 1 time for restaurant Severo

    # Silva wins!
    Given now it's 12:00
    Then the chosen restaurant is Silva

    # Next day
    Given today is 18/09/2018

    # Can't accept a vote for Silva this week
    When hungryUser1 attempts to vote 1 time for restaurant Silva
    Then it has 0 votes in the database

    # Neither can it accept from user 2
    When hungryUser2 attempts to vote 1 time for restaurant Silva
    Then it has 0 votes in the database

    # Sushi is ok, hasn't been chosen this week!
    When hungryUser3 attempts to vote 1 time for restaurant Sushi
    Then it has 1 vote in the database

    # Sushi wins
    Given now it's 12:00
    Then the chosen restaurant is Sushi
