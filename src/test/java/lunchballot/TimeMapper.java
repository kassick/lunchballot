package lunchballot;

import cucumber.api.Transformer;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeMapper extends Transformer<LocalTime> {
    @Override
    public LocalTime transform(String time) {

        //Not too sure about the date pattern though, check it out if it gives correct result
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        return LocalTime.parse(time, formatter);
    }
}
