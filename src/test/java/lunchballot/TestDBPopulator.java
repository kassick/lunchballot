package lunchballot;

import lunchballot.ballot.model.Restaurant;
import lunchballot.ballot.repositories.BallotResultRepository;
import lunchballot.ballot.repositories.RestaurantRepository;
import lunchballot.ballot.repositories.VotesRepository;
import lunchballot.user.model.HungryUser;
import lunchballot.user.repository.HungryUserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;

public class TestDBPopulator {

    /*
      Populates the database with fake values
     */
    public
    static void populateDb(int nusers,
                           HungryUserRepository hur,
                           RestaurantRepository rr,
                           VotesRepository vr,
                           BallotResultRepository brr,
                           PasswordEncoder passwordEncoder) {
        brr.deleteAll();
        vr.deleteAll();
        rr.deleteAll();
        hur.deleteAll();

        populateUserRepository(nusers, hur, passwordEncoder);
        populateRestaurants(rr);
    }

    static void populateUserRepository(int nusers, HungryUserRepository hungryUserRepository, PasswordEncoder passwordEncoder) {
        hungryUserRepository.save(
                new HungryUser(
                        "admin",
                        passwordEncoder.encode("admin"),
                        HungryUser.ROLE_ADMIN));

        for (int i = 1; i <= nusers; i++) {
            hungryUserRepository.save(
                    new HungryUser(
                            String.format("hungryUser%d", i),
                            passwordEncoder.encode("senha123"),
                            HungryUser.ROLE_USER
                    )
            );
        }
    }

    static void populateRestaurants(RestaurantRepository restaurantRepository) {
        restaurantRepository.save(new Restaurant("Vitória"));
        restaurantRepository.save(new Restaurant("Silva"));
        restaurantRepository.save(new Restaurant("40 (PUC)"));
        restaurantRepository.save(new Restaurant("Palatus (PUC)"));
        restaurantRepository.save(new Restaurant("Pé de Manga"));
        restaurantRepository.save(new Restaurant("Bar Maza"));
        restaurantRepository.save(new Restaurant("Novo Sabor"));
        restaurantRepository.save(new Restaurant("Severo Garage (PUC)"));
        restaurantRepository.save(new Restaurant("Sushi"));
    }
}
