package lunchballot;

import lunchballot.time.Week;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;


public class WeekUnitTest
{
    @Test
    public void test_week_starts_on_monday()
    {
        LocalDate wednesday = LocalDate.of(2018, 12, 5);

        assert wednesday.getDayOfWeek().equals(DayOfWeek.WEDNESDAY);

        Week w = new Week(wednesday);

        assert w.getStart().equals(LocalDate.of(2018,12,3));

        assert w.getStart().getDayOfWeek().equals(DayOfWeek.MONDAY);
    }

    @Test
    public void test_week_ends_on_sunday()
    {
        LocalDate wednesday = LocalDate.of(2018, 12, 5);

        Week w = new Week(wednesday);

        assert w.getEnd().getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    @Test(expected = NullPointerException.class)
    public void test_null_date_throws()
    {
        Week w = new Week(null);
    }
}
