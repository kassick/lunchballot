package lunchballot;

import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.LunchTimeManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class Stepdefs extends LunchBallotTestConfiguration {

    @Autowired
    LunchTimeManager lunchTimeManager; //= lunchTimeManager; //new LunchTimeManagerImpl();
    String answer = "";

    @Given("^it's (.*?)$")
    public void it_s(@Transform(TimeMapper.class) LocalTime time) throws Exception {
        LocalDateTime now = lunchTimeManager.now();
        LocalDateTime new_now = now.with(time);

        ((LunchTimeManagerImpl)lunchTimeManager).timeTravelTo(new_now);
    }

    @Given("^now is midnight$")
    public void now_it_midnight() throws Exception {
        LocalDateTime now = lunchTimeManager.now();
        LocalTime midnight = LocalTime.of(0, 0);
        LocalDateTime new_now = now
                .with(midnight)
                .plusDays(1);

        ((LunchTimeManagerImpl)lunchTimeManager).timeTravelTo(new_now);
    }


    @When("^I ask if it's lunch time$")
    public void i_ask_if_it_s_lunch_time() throws Exception {
        if (lunchTimeManager.isLunchTime()) {
            answer = "yes";
        } else {
            answer = "not yet";
        }
    }

    @Then("^I should be told \"([^\"]*)\"$")
    public void i_should_be_told(String expected) throws Exception {
        assert (expected.equals(answer));
    }

}