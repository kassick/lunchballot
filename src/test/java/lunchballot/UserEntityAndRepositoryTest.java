package lunchballot;

import lunchballot.ballot.model.Restaurant;
import lunchballot.ballot.model.Vote;
import lunchballot.ballot.model.VoteId;
import lunchballot.time.DateUtils;
import lunchballot.user.model.HungryUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class UserEntityAndRepositoryTest extends LunchBallotTestConfiguration {

    @Before
    public void setup() {
        TestDBPopulator.populateDb(10,
                userRepository,
                restaurantRepository,
                votesRepository,
                ballotResultRepository,
                passwordEncoder);
    }

    @Test
    public void test_can_update_user() {
        Optional<HungryUser> user1 = userRepository.findById("hungryUser1");
        assert user1.isPresent();

        HungryUser u = user1.get();

        assert u.getRole().equals(HungryUser.ROLE_USER);

        u.setRole(HungryUser.ROLE_ADMIN);

        userRepository.save(u);

        Optional<HungryUser> user2 = userRepository.findById("hungryUser1");

        assert user2.isPresent();

        HungryUser u2 = user2.get();

        assert u2.getRole().equals(u.getRole());
    }

    @Test
    public void test_cascade_delete_user() {
        Optional<HungryUser> ou1 = userRepository.findById("hungryUser1");
        List<Restaurant> or1 = restaurantRepository.findByNameContainingIgnoreCase("40");

        assert ou1.isPresent(); // these fake users and restaurants
        assert or1.size() == 1; // were populated, they MUST be here

        LocalDate d = LocalDate.now();
        String name = ou1.get().getName();
        Date date = DateUtils.localDateToDate(d);

        votesRepository.save(
                new Vote(
                        new VoteId(
                                ou1.get(),
                                DateUtils.localDateToDate(d)),
                        or1.get(0)
                )
        );


        Optional<Vote> v = votesRepository.findByVoteId_hungryUser_nameAndVoteId_date(
                name, date
        );

        // MUST be present
        assert v.isPresent();
        assert v.get().getRestaurant().getId().equals(or1.get(0).getId());

        // Now delete it
        userRepository.delete(ou1.get());

        v = votesRepository.findByVoteId_hungryUser_nameAndVoteId_date(
                name, date
        );

        assert !v.isPresent();
    }

}
