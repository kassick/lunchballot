package lunchballot;

import lunchballot.ballot.BallotManager;
import lunchballot.controllers.LunchTimeController;
import lunchballot.security.HungryUserPrincipal;
import lunchballot.time.LunchTimeManager;
import lunchballot.user.model.HungryUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/*
    Test Case: Access the LunchTimeController after decision time when there has been no vote
 */


@RunWith(SpringRunner.class)
@WebAppConfiguration
public class LunchTimeControllerNoVoteTest
        extends LunchBallotTestConfiguration {

    @Autowired
    WebApplicationContext context;

    @Autowired
    LunchTimeManager lunchTimeManager;

    @Autowired
    BallotManager ballotManager;

    @Autowired
    LunchTimeController lunchTimeController;

    MockMvc mockMvc;

    LocalDate today, tomorrow;

    UserDetails userDetails;

    @Before
    public void setup() {

        today = LocalDate.now();
        tomorrow = today.plusDays(1);

        TestDBPopulator.populateDb(3,
                userRepository,
                restaurantRepository,
                votesRepository,
                ballotResultRepository,
                passwordEncoder);

        mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(context)
                        .apply(SecurityMockMvcConfigurers.springSecurity())
                        .build();

        Optional<HungryUser> user = userRepository.findById("hungryUser1");

        assert user.isPresent();

        userDetails = new HungryUserPrincipal(user.get());
    }


    @Test
    public void test_today_get_when_logged_in_and_lunchtime() throws Exception {
        lunchTimeManager.executeSerialized(
                (tm) -> {
                    tm.timeTravelTo(today.atStartOfDay()
                            .withHour(12)
                            .withMinute(0)
                            .withSecond(0));

                    return false;
                });

        ResultActions r0 = mockMvc.perform(
                get("/")
                        .with(csrf())
                        .with(user(userDetails)));

        MvcResult r1 = r0.andExpect(status().isOk()).andReturn();

        Map<String, Object> model = r1.getModelAndView().getModel();

        int x = r1.getResponse().getContentAsString().length();
        // Make sure model has sane data

        // the same username as saved in details
        assert model
                .getOrDefault("username", "")
                .equals(userDetails.getUsername());

        // lunch time!!!
        assert model.get("lunchTime").equals(true);

        // User has not voted
        assert !model.containsKey("userHasVoted");

        // Must have available restaurants and none unavailable
        assert !model.containsKey("availableRestaurants");
        assert !model.containsKey("unavailableRestaurants");

        // no votes, restaurant should be null
        assert model.get("restaurant") == null;
    }
}
