package lunchballot;

import lunchballot.ballot.BallotManager;
import lunchballot.ballot.model.BallotResultProjection;
import lunchballot.ballot.model.Restaurant;
import lunchballot.controllers.LunchTimeController;
import lunchballot.security.HungryUserPrincipal;
import lunchballot.time.LunchTimeManager;
import lunchballot.user.model.HungryUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
   Test Cases:

   - Cast votes for a restaurant and then check the result (votes on 40)
   - Cast votes for a restaurant and try to vote after the restaurant has been chosen

 */

@RunWith(SpringRunner.class)
@WebAppConfiguration
public class LunchTimeAndVoteControllerIntegrationTest
        extends LunchBallotTestConfiguration {

    @Autowired
    WebApplicationContext context;

    @Autowired
    LunchTimeManager lunchTimeManager;

    @Autowired
    BallotManager ballotManager;

    @Autowired
    LunchTimeController lunchTimeController;

    MockMvc mockMvc;

    LocalDate today;

    UserDetails userDetails;

    @Before
    public void setup() {

        today = LocalDate.now();

        mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(context)
                        .apply(SecurityMockMvcConfigurers.springSecurity())
                        .build();

        Optional<HungryUser> user = userRepository.findById("hungryUser1");

        assert user.isPresent();

        userDetails = new HungryUserPrincipal(user.get());
    }

    public void populate_database() {
        TestDBPopulator.populateDb(3,
                userRepository,
                restaurantRepository,
                votesRepository,
                ballotResultRepository,
                passwordEncoder);
    }

    @Test
    public void vote_and_lunch_time_integration_test() throws Exception {

        populate_database();

        time_is_today_before_lunch();

        Restaurant chosenOne = everybode_wants_to_eat_at_40();

        time_is_today_lunchtime();

        ballotManager.chooseRestaurant();

        // now access the home as some user
        ResultActions r0 = mockMvc.perform(
                get("/")
                        .with(csrf())
                        .with(user(userDetails)));

        MvcResult r1 = r0.andExpect(status().isOk()).andReturn();

        Map<String, Object> model = r1.getModelAndView().getModel();

        // Make sure model has sane data

        // the same username as saved in details
        assert model
                .getOrDefault("username", "")
                .equals(userDetails.getUsername());

        // lunch time!!!
        assert model.get("lunchTime").equals(true);

        // On lunch time, model does not reply with hasVoted
        assert !model.containsKey("userHasVoted");

        // available/unavailable restaurants do not
        assert !model.containsKey("availableRestaurants");
        assert !model.containsKey("unavailableRestaurants");

        // no votes, restaurant should be null
        assert model.get("restaurant").equals(chosenOne.getName());

        List<BallotResultProjection> br =
                (List<BallotResultProjection>) model.get("ballotResults");

        // Now check results:
        // 3 users voted on 40, so, 40 must have 3 votes
        assert br.size() > 0;
        assert br.get(0).getRestaurantName().equals(chosenOne.getName());
        assert br.get(0).getNVotes().equals(userRepository.count());
    }

    @Test
    public void test_vote_again() throws Exception {
        populate_database();

        time_is_today_before_lunch();

        Restaurant chosenOne = everybode_wants_to_eat_at_40();
        int restaurantId = chosenOne.getId().intValue();

        // Admin tries to vote again
        HungryUser u = userRepository.findById("admin").get();
        UserDetails curUserDetails = new HungryUserPrincipal(u);
        MvcResult result =
                mockMvc.perform(
                        get(String.format(
                                "/vote?restaurantId=%d",
                                restaurantId))
                                .with(csrf())
                                .with(user(curUserDetails)))
                        .andExpect(status().isOk()).andReturn();

        Map<String, Object> model = result.getModelAndView().getModel();
        int x = result.getResponse().getContentAsString().length();

        assert ((String) model.get("error")).contains("já votou");
    }

    @Test
    public void test_vote_after_ballot_closes() throws Exception {
        populate_database();

        time_is_today_before_lunch();

        Restaurant chosenOne = everybode_wants_to_eat_at_40();
        int restaurantId = chosenOne.getId().intValue();

        time_is_today_lunchtime();

        // Admin tries to vote past lunchtime
        HungryUser u = userRepository.findById("admin").get();
        UserDetails curUserDetails = new HungryUserPrincipal(u);
        MvcResult result =
                mockMvc.perform(
                        get(String.format(
                                "/vote?restaurantId=%d",
                                restaurantId))
                                .with(csrf())
                                .with(user(curUserDetails)))
                        .andExpect(status().isOk()).andReturn();

        Map<String, Object> model = result.getModelAndView().getModel();
        int x = result.getResponse().getContentAsString().length();

        assert ((String) model.get("error")).contains("encerrou");
    }

    @Test
    public void test_vote_again_on_40_tomorrow() throws Exception {
        populate_database();

        time_is_today_before_lunch();

        Restaurant chosenOne = everybode_wants_to_eat_at_40();
        int restaurantId = chosenOne.getId().intValue();

        time_is_today_lunchtime();

        ballotManager.chooseRestaurant();

        time_is_tomorrow_before_lunch();

        // Admin tries to vote on 40 again. I'm not made of money
        HungryUser u = userRepository.findById("admin").get();
        UserDetails curUserDetails = new HungryUserPrincipal(u);
        MvcResult result =
                mockMvc.perform(
                        get(String.format(
                                "/vote?restaurantId=%d",
                                restaurantId))
                                .with(csrf())
                                .with(user(curUserDetails)))
                        .andExpect(status().isOk()).andReturn();

        Map<String, Object> model = result.getModelAndView().getModel();
        int x = result.getResponse().getContentAsString().length();

        assert ((String) model.get("error")).contains("Não é possível votar nesse restaurante");
    }

    public void time_is_today_lunchtime() throws Exception {
        lunchTimeManager.executeSerialized(
                (tm) -> {
                    tm.timeTravelTo(
                            today.atStartOfDay()
                                    .withHour(12)
                                    .withMinute(0)
                                    .withSecond(0));
                    return false;
                });
    }

    public void time_is_today_before_lunch() throws Exception {
        lunchTimeManager.executeSerialized(
                (tm) -> {
                    tm.timeTravelTo(
                            today.atStartOfDay());

                    return false;
                });
    }

    public void time_is_tomorrow_before_lunch() throws Exception {
        lunchTimeManager.executeSerialized(
                (tm) -> {
                    tm.timeTravelTo(
                            today.plusDays(1)
                                    .atStartOfDay());

                    return false;
                });
    }


    public Restaurant everybode_wants_to_eat_at_40() throws Exception {
        List<Restaurant> rl = restaurantRepository.findByNameContainingIgnoreCase("40");
        assert rl.size() == 1;
        Restaurant chosenOne = rl.get(0);
        int restaurantId = rl.get(0).getId().intValue();

        // Cast a vote for each user
        for (HungryUser u : userRepository.findAll()) {
            UserDetails curUserDetails = new HungryUserPrincipal(u);
            MvcResult result =
                    mockMvc.perform(
                            get(String.format(
                                    "/vote?restaurantId=%d",
                                    restaurantId))
                                    .with(csrf())
                                    .with(user(curUserDetails)))
                            .andExpect(status().isOk()).andReturn();
            int x = result.getResponse().getContentAsString().length();
        }
        return chosenOne;
    }




}
