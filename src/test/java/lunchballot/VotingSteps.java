package lunchballot;

import cucumber.api.Transform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lunchballot.ballot.BallotManager;
import lunchballot.ballot.model.BallotResultProjection;
import lunchballot.ballot.model.Restaurant;
import lunchballot.ballot.model.Vote;
import lunchballot.exceptions.UnknownUserException;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.LunchTimeManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class VotingSteps extends LunchBallotTestConfiguration {

    static final int DEFAULT_NUSERS = 5;

    @Autowired
    public BallotManager ballotManager;

    @Autowired
    public LunchTimeManager lunchTimeManager;

    private List<String> currentVotedFor = new ArrayList<>();
    private String currentUserName = null;
    private List<BallotResultProjection> currentVotingResults = null;

    @Given("^now it's (.*?)$")
    public void now_it_s(@Transform(TimeMapper.class) LocalTime time) throws Exception {
        LocalDateTime now = lunchTimeManager.now();
        LocalDateTime new_now = now.with(time);

        ((LunchTimeManagerImpl)lunchTimeManager).timeTravelTo(new_now);
    }

    @Given("^today is (\\d+)/(\\d+)/(\\d+)$")
    public void today_is(int day, int month, int year) {
        LocalDate today = LocalDate.of(year, month, day);
        ((LunchTimeManagerImpl)lunchTimeManager).timeTravelTo(today.atStartOfDay());
    }

    @Given("^the database is freshly populated$")
    public void database_is_freslh_populated() throws Exception {
        TestDBPopulator.populateDb(
                DEFAULT_NUSERS,
                userRepository, restaurantRepository, votesRepository,
                ballotResultRepository, passwordEncoder);
    }

    @Given("^the database is freshly populated with (\\d+) users$")
    public void database_is_freslh_populated(int nusers) throws Exception {
        TestDBPopulator.populateDb(
                nusers,
                userRepository, restaurantRepository, votesRepository,
                ballotResultRepository, passwordEncoder);
    }

    @When("^(.*?) attempts to vote (\\d+) times? for restaurant (.*?)$")
    public void hungryuser_attempts_to_vote(String username, int times, String name) throws Exception {

        if (currentUserName != null && !currentUserName.equals(username)) {
            // We're changing the user that's voting!
            currentVotedFor = new ArrayList<>();
        }

        currentUserName = username;

        Restaurant r = findRestaurantMatching(name);

        for (int i = 0; i < times; i++) {
            try {
                ballotManager.castVoteForUser(currentUserName, r);
                currentVotedFor.add(r.getName());
            } catch (UnknownUserException e) {
                // Ooops, invalid user in the tests!
                throw e;
            } catch (Exception e) {
                // All the rest: ignore
                // log.info("\n>>>>>>>>>   Got a " + e.toString() + "<<<<<<<<<<\n");
            }
        }
    }

    @Then("^it has (\\d+) votes? in the database$")
    public void hungryuser_has_vote_in_the_database(int nvotes) throws Exception {
        assert (currentVotedFor.size() == nvotes);

        Optional<Vote> ov = votesRepository.findByVoteId_hungryUser_nameAndVoteId_date(currentUserName, lunchTimeManager.todayAsDate());

        if (nvotes == 0)
            assert !ov.isPresent();
        else if (nvotes == 1)
            assert ov.isPresent();
        else
            throw new Exception("There's no way a user can vote more then once!");
    }

    @Then("^its vote is for restaurant (.*?)$")
    public void hungryuser_has_voted_for(String name) throws Exception {

        Optional<Vote> ov = votesRepository.findByVoteId_hungryUser_nameAndVoteId_date(currentUserName, lunchTimeManager.todayAsDate());

        // if name is NONE, then r should be null
        if (name.equals("NONE"))
            assert (!ov.isPresent());
        else {
            Restaurant r = findRestaurantMatching(name);
            // It gave the name of a restaurant. Make sure we voted for it
            assert r != null && currentVotedFor.contains(r.getName());

            // Just in case, make sure that the restaurant found matches the id of the vote
            assert ov.isPresent() && r.getId().equals(ov.get().getRestaurant().getId());
        }
    }


    @Then("^the chosen restaurant is (.*?)$")
    public void the_chosen_restaurant_is(String name) throws Exception {

        ballotManager.chooseRestaurant();

        Optional<Restaurant> chosenOne = ballotManager.getBallotResult();

        // NONE => No restaurant was selected
        if (name.equals("NONE"))
            assert !chosenOne.isPresent();
        else {

            assert chosenOne.isPresent();

            Restaurant r = findRestaurantMatching(name);

            assert chosenOne.get().getId().equals(r.getId());
        }
    }

    @When("^the votes for today are calculated$")
    public void calculateVotesForToday() throws Exception {
        currentVotingResults = votesRepository.getBallotResultsFor(lunchTimeManager.todayAsDate());
    }

    @Then("^restaurant (.*?) has (\\d+) votes")
    public void verify_number_of_votes_for(String name, int nvotes)
            throws Exception {
        String lowerName = name.toLowerCase();

        for (BallotResultProjection br : currentVotingResults) {
            String curRestaurantName = br.getRestaurantName().toLowerCase();
            if (curRestaurantName.indexOf(lowerName) >= 0) {
                assert br.getNVotes().intValue() == nvotes;

                return;
            }
        }

        throw new Exception("No match for " + name + " in current results");
    }

    @Then("^choice for (\\d+)/(\\d+)/(\\d+) was (.*?)$")
    public void choice_for_date_was(int day, int month, int year, String name) throws Exception {
        LocalDate date = LocalDate.of(year, month, day);

        Optional<Restaurant> or = ballotManager.getBallotResultForDate(date);

        if (name.equals("NONE"))
            assert !or.isPresent();
        else {
            assert or.isPresent();

            Restaurant r = findRestaurantMatching(name);

            assert r.getId().equals(or.get().getId());
        }
    }

    private Restaurant findRestaurantMatching(String name) throws Exception {
        List<Restaurant> lr = restaurantRepository.findByNameContainingIgnoreCase(name);
        if (lr.size() == 0)
            throw new Exception("No restaurant matching " + name);
        else if (lr.size() > 1)
            throw new Exception("Restaurant " + name + " is too generic ...");
        else
            return lr.get(0);

    }

}