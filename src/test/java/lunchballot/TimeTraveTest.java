package lunchballot;

import lunchballot.time.LunchTimeManagerImpl;
import org.junit.Test;

import java.time.LocalDate;
import java.time.temporal.TemporalField;

import static java.time.temporal.ChronoUnit.DAYS;

public class TimeTraveTest {

    @Test
    public void test_time_travel_to_next_day() {
        LunchTimeManagerImpl ltm = new LunchTimeManagerImpl();

        LocalDate today = ltm.today();
        ltm.timeTravelToNextDay();

        assert DAYS.between(today, ltm.today()) == 1;
    }
}
