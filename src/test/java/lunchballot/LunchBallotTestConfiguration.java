package lunchballot;

import lunchballot.ballot.BallotManager;
import lunchballot.ballot.repositories.BallotResultRepository;
import lunchballot.ballot.repositories.RestaurantRepository;
import lunchballot.ballot.repositories.VotesRepository;
import lunchballot.time.LunchTimeManager;
import lunchballot.time.LunchTimeManagerImpl;
import lunchballot.user.repository.HungryUserRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;


//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = LunchBallotApplication.class
        //webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
         )
@ContextConfiguration
public class LunchBallotTestConfiguration {

    @Bean(name = "lunchTimeManager")
    @Scope("singleton")
    public LunchTimeManager lunchTimeManager() {
        return new LunchTimeManagerImpl();
    }

    @Bean
    public BallotManager ballotManager() {
        return new BallotManager();
    }

    @Autowired
    public HungryUserRepository userRepository;

    @Autowired
    public RestaurantRepository restaurantRepository;

    @Autowired
    public VotesRepository votesRepository;

    @Autowired
    public BallotResultRepository ballotResultRepository;

    @Autowired
    public PasswordEncoder passwordEncoder;

    @Test
    public void nope() {
        assert true;
    }
}

/*
@ContextConfiguration(
  classes = SpringDemoApplication.class,
  loader = SpringApplicationContextLoader.class)
@WebAppConfiguration
@IntegrationTest
public class LunchBallotTestConfiguration {

}
 */