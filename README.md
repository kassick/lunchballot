# Desafio de Programação DBServer

O seguinte código implementa o sistema de escolha de restaurante para almoço, com restrição de apenas um voto por dia por usuário e sem permitir repetição de restaurantes na semana.

A implementação foi feita com Spring MVC, JPA e Spring-Security.

## Armazenamento dos Dados

Os dados são armazenados com JPA em uma base H2 em memória. Para algumas consultas, foram utilizadas queries customizadas (via anotação Query), enquanto para outras foi utilizado o sistema padrão do JPA de findByFieldAndOtherField.

As entidades estão organizadas nos seguintes pacotes:

- lunchballot.user.{model, repository} : Contém classes de entidade e repositório para *usuários*. A tabela de usuários armazena *nome*, *hash de senha* e o papel do usuário no sistema (`USER` ou `ADMIN`).

- ballot.{model, repositories} : Contém classes de entidade de repositórios para as seguintes tabelas>

    + `Restaurants` : Tabela de restaurantes (apenas id e nome).

    + `Votes` : Tabela que registra os votos de cada usuário. Armazena nome de usuário, data e código de restaurante. Possui chave primária por usuário e data, auxiliando no controle de votos repetidos.

    + `BallotResults` : Registra o restaurante escolhido para cada data (data, id de restaurante

## Lógica de Sistema

A lógica do sistema está implementada na classe `ballot.BallotManager`. As verificações de votos repetidos no dia e de quais restaurantes podem ser votados são todas feitas nos métodos dessa classe.

A BallotManager trabalha conjuntamente com a LunchTimeManager. Esta última simula um "relógio" para controlar se é ou não possível votar em algum restaurante. Ela começou como uma espécie de *mock** para utilizar em testes posteriores, mas preferi manter seu uso no sistema, pois isso tornaria mais fácil os testes automáticos e o também o desenvolvimento da interface web.

## Funcionamento do Sistema

### Estória 1

O profissional faminto acessa a URL do sistema e entra com seu usuário e senha. Se este acesso for anterior ao *horário de almoço* (definido no sistema como 11h50am**, ele verá uma lista de restaurantes nos quais ele pode votar. Os restaurantes que já foram visitados na semana aparecem ao fim da lista com *strike-though*.

Após registrar o voto, o sistema não apresenta mais as opções de voto para o usuário. Se mesmo assim o usuário enviar requisições diretamente ao servidor, a classe BallotManager irá detectar o voto em duplicado e descartar o novo voto.

### Estória 2

As consultas feitas pelos CrudRepositores (getAvailableRestaurants e getUnavailableRestaurant) já categorizam quais restaurantes estão disponíveis para voto na data. A interface de votação não oferece para votação os restaurantes visitados na semana.

Se mesmo assim o usuário enviar requisições para o servidor HTTP, o BallotManager irá detectar que o restaurante já foi visitado naquela semana e não irá registrar o voto.

### Estória 3

Se o usuário acessar o tempo depois do "horário de almoço" (11h50am), o sistema irá apresentar o resultado da votação e indicará qual o restaurante escolhido. Após o "horário de almoço", não são mais aceitos votos até o dia seguinte (00h00 do dia seguinte).

## Usuário Admin

O sistema possui um usuário *admin* que tem acesso a uma interface que controla o "relógio" do sistema. Esta interface foi criada apenas para testes (além dos automatizados). Ela encontra-se disponível no caminho /admin do servidor (apenas para usuário admin).

## Testes Automatizados

O sistema está com 88% de cobertura por testes automatizados.

Os testes de comportamento da lógica do BallotManager estão implementados como estórias em Gherkin e são executadas com Cucumber (ver StepDefs e VotingSteps em tests/java/lunchballot, além dos features em tests/resources/lunchballot).

Testes de classes isoladas e utilitárias, bem como os testes automatizados dos objetos controller, foram feitas com JUnit (também em tests/java/lunchballot).

# Requisitos para Execução

É necessário um navegador web e do compilador da linguagem Java 8 ou maior. A compilação / execução pode ser feita tanto na máquina local quanto em um container docker.

## Execução na máquina local:

Requisitos:

- Navegador

- Java 1.8 (JDK)

- Maven

Para executar, execute os comandos abaixo e acesse o endereço http://localhost:8080

```sh
git clone https://bitbucket.org/kassick/lunchballot/

mvn test

mvn spring-boot:run
```

## Execução Via Docker

No diretório `docker` deste projeto, encontra-se um arquivo Dockerfile. Com um shell neste diretório, execute os comandos abaixo e acesse a URL http://localhost:8080

```sh

docker build . -t lunchballot

docker run -p 8080:8080 lunchballot

```

## Acesso de Administrador

Com o sistema em execução, acesse a url http://localhost:8080/admin e entre com as credenciais do usuário admin.

A interface de gerenciamento permite:

- Ajustar a "hora" para qualquer hora do dia. Isto tem o efeito de abrir e fechar a votação para o dia.

- Avançar para o próximo dia

**ATENÇÃO:** Ao ajustar a hora para um horário depois do almoço (11h50), um restaurante é escolhido e esta escolha é registrada na base. Porém, se depois de escolhido um restaurante o administrador forçar o sistema a "voltar no tempo" para antes do almoço, o restaurante escolhido permanece na base de dados. Assim, o sistema não permitirá votar no restaurante novamente.

  Para testar a votação novamente, use o botão "próximo dia".

## Usuários e Senhas

O sistema exige usuário e senha para registrar votos e acompanhar os resultados da votação. Os seguintes usuários são criados na inicialização para fins de teste.

- admin / admin

- hungry1 / senha123

- hungry2 / senha123

- hungry3 / senha123

- hungry4 / senha123

- hungry5 / senha123

# Melhorias Possíveis

## Prioridade Alta

- A *data/hora* do sistema faz parte do estado transacional. Se um usuário estava votando quando chegou a hora do almoço, seu voto não deve ser ignorado (salvo em Votes, mas não contabilizado na hora de decidir pelo restaurante). Para evitar isso, várias das operações do ballotmanager são serializadas para executar de forma atômica. Uma solução melhor seria utilizar transações que integrem o estado da hora e sofram rollback caso tenha sido decidido um restaurnte.

- Usar Transactional de maneira correta.

- Mover as strings de mensagens para um arquivo de resources e usar i18n/l10n. Os testes unitários/integração dos controladores usam strings hardcoded em pt_BR -- longe do ideal.

- Mais testes automatizados. O Controller de administração está sem cobertura de testes.

- Reorganizar os testes. Da maneira como eles foram projetados, a execução dos testes é demorada pois a base de dados é frequentemente re-populada. Reorganizar eles em situações mais estáveis (e.g. pristine-before-lunch, dirty-before-lunch, lunch-time-no-vote, lunch-time-vote) evitaria tantas reinicializações da base.

## Prioridade Média

- Interface web precisa de trabalho (cue http://www.smbc-comics.com/comic/2012-05-01)

- Utilizar LDAP como armazenamento de usuários. Não o fiz por que não queria depender de uma ferramenta externa -- só descobri o LDAP embedded do Spring depois de criar toda a base de dados.

## Prioridade Baixa

- Melhor descrição do restaurante (e.g. localização, link para facebook, tripadvisor, etc., tabela de pratos oferecidos, etc.)

- CRUD para gerenciar restaurantes

- Uma página com estatísticas dos restaurantes escolhidos nas semanas anteriores
